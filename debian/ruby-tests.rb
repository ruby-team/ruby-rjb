# The test needs to know where to find the class files
ENV['CLASSPATH'] = File.expand_path('../../test', __FILE__)

if ENV['ADTTMP'].nil? # not running through autopkgtest
  # 'data' directory is not yet installed, so use the local version
  ENV['RJB_BRIDGE_FILE'] = File.expand_path('../../data/rjb/jp/co/infoseek/hp/arton/rjb/RBridge.class', __FILE__)
end

require 'test/test.rb'
